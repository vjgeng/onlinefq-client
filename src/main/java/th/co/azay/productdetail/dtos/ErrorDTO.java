package th.co.azay.productdetail.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = ErrorDTO.Builder.class)
public class ErrorDTO {

	private final String field;
	private final LocalizedStringDTO userMessage;
	private final String internalMessage;

	public String getField() {
		return field;
	}

	public LocalizedStringDTO getUserMessage() {
		return userMessage;
	}

	public String getInternalMessage() {
		return internalMessage;
	}

	public ErrorDTO(Builder builder) {
		this.field = builder.field;
		this.userMessage = builder.userMessage;
		this.internalMessage = builder.internalMessage;
	}

	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "with")
	public static class Builder {

		private String field;
		private LocalizedStringDTO userMessage;
		private String internalMessage;

		public Builder withField(String field) {
			this.field = field;
			return this;
		}

		public Builder withUserMessage(LocalizedStringDTO userMessage) {
			this.userMessage = userMessage;
			return this;
		}

		public Builder withInternalMessage(String internalMessage) {
			this.internalMessage = internalMessage;
			return this;
		}

		public ErrorDTO build() {
			return new ErrorDTO(this);
		}

	}

	@Override
	public String toString() {
		return "ErrorDTO [field=" + field + ", userMessage=" + userMessage + ", internalMessage=" + internalMessage + "]";
	}

}
