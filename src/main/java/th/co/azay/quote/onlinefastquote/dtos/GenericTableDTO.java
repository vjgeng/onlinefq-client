package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.ZonedDateTime;
import java.util.Map;

import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class GenericTableDTO {

	private String code;
	private String parentCode;
	private ZonedDateTime lastModifiedDate;
	private Map<String, String> extendedProperties;
	private String countryCode;
	private String channelCode;
	private String type;
	private LocalizedStringDTO name = new LocalizedStringDTO();
	private LocalizedStringDTO description = new LocalizedStringDTO();

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Map<String, String> getExtendedProperties() {
		return extendedProperties;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	public ZonedDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public void setExtendedProperties(Map<String, String> extendedProperties) {
		this.extendedProperties = extendedProperties;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public LocalizedStringDTO getName() {
		return name;
	}

	public void setName(LocalizedStringDTO name) {
		this.name = name;
	}

	public LocalizedStringDTO getDescription() {
		return description;
	}

	public void setDescription(LocalizedStringDTO description) {
		this.description = description;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

}
