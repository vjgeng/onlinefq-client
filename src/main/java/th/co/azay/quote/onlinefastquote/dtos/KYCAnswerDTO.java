package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KYCAnswerDTO {

	private String moduleCode;
	private String point;
	private String q1;
	private String q2;
	private String q3;
	private String q4;
	private String q5;
	private String q6;

	@JsonInclude(Include.ALWAYS)
	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	@JsonInclude(Include.ALWAYS)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	@JsonInclude(Include.ALWAYS)
	public String getQ1() {
		return q1;
	}

	public void setQ1(String q1) {
		this.q1 = q1;
	}

	@JsonInclude(Include.ALWAYS)
	public String getQ2() {
		return q2;
	}

	public void setQ2(String q2) {
		this.q2 = q2;
	}

	@JsonInclude(Include.ALWAYS)
	public String getQ3() {
		return q3;
	}

	public void setQ3(String q3) {
		this.q3 = q3;
	}

	@JsonInclude(Include.ALWAYS)
	public String getQ4() {
		return q4;
	}

	public void setQ4(String q4) {
		this.q4 = q4;
	}

	@JsonInclude(Include.ALWAYS)
	public String getQ5() {
		return q5;
	}

	public void setQ5(String q5) {
		this.q5 = q5;
	}

	@JsonInclude(Include.ALWAYS)
	public String getQ6() {
		return q6;
	}

	public void setQ6(String q6) {
		this.q6 = q6;
	}

}
