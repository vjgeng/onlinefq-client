package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(value = { "sortOrder" })
public class ProductDetailDTO {

    private int sortOrder;
    private String code;
    private boolean enabled;
    private boolean display;
    private int minAge;
    private int maxAge;
    private String paymentFrequency; //Added by Panut
    private List<ProductRules> rules;
    private List<RiderDetails> riderDetails;
    private List<RiderDetails> additionalRiderDetails;

    @Data
    @Builder
    public static class ProductRules {
        private String gender;
        private int minAge;
        private int maxAge;
        private String occupation;
        private List<RiderDetails> riderDetails;
        private List<RiderDetails> additionalRiderDetails;
    }

    @Data
    @Builder
    public static class RiderDetails {
        private String code;
        private BigDecimal sumAssured;
        private List<RiderDetailsOptions> options;

         public String getExactRiderCode() {
             return (code.contains("_")) ? code.split("_")[0] : code;
         }
    }

    @Data
    @Builder
    public static class RiderDetailsOptions {
        private String key;
        private String value;
        private String type;
    }
    
    public String toString() {
    	return "";
    }
}