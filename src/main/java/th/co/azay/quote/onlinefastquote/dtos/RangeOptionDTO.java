package th.co.azay.quote.onlinefastquote.dtos;

public class RangeOptionDTO {

	private Object min;
	private Object max;

	public RangeOptionDTO(Object min, Object max) {
		this.min = min;
		this.max = max;
	}

	public Object getMin() {
		return min;
	}

	public void setMin(Object min) {
		this.min = min;
	}

	public Object getMax() {
		return max;
	}

	public void setMax(Object max) {
		this.max = max;
	}

}
