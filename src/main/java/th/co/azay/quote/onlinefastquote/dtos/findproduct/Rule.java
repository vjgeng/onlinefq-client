
package th.co.azay.quote.onlinefastquote.dtos.findproduct;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "gender",
    "minAge",
    "maxAge",
    "occupation",
    "riderDetails",
    "additionalRiderDetails"
})
public class Rule {

    @JsonProperty("gender")
    private String gender;
    @JsonProperty("minAge")
    private Integer minAge;
    @JsonProperty("maxAge")
    private Integer maxAge;
    @JsonProperty("occupation")
    private String occupation;
    @JsonProperty("riderDetails")
    private List<RiderDetail> riderDetails = null;
    @JsonProperty("additionalRiderDetails")
    private List<AdditionalRiderDetail> additionalRiderDetails = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("minAge")
    public Integer getMinAge() {
        return minAge;
    }

    @JsonProperty("minAge")
    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    @JsonProperty("maxAge")
    public Integer getMaxAge() {
        return maxAge;
    }

    @JsonProperty("maxAge")
    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    @JsonProperty("occupation")
    public String getOccupation() {
        return occupation;
    }

    @JsonProperty("occupation")
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @JsonProperty("riderDetails")
    public List<RiderDetail> getRiderDetails() {
        return riderDetails;
    }

    @JsonProperty("riderDetails")
    public void setRiderDetails(List<RiderDetail> riderDetails) {
        this.riderDetails = riderDetails;
    }

    @JsonProperty("additionalRiderDetails")
    public List<AdditionalRiderDetail> getAdditionalRiderDetails() {
        return additionalRiderDetails;
    }

    @JsonProperty("additionalRiderDetails")
    public void setAdditionalRiderDetails(List<AdditionalRiderDetail> additionalRiderDetails) {
        this.additionalRiderDetails = additionalRiderDetails;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
