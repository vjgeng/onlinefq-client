package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = ModuleDetailCategoryDTO.Builder.class)
public class ModuleDetailCategoryDTO {

	private final String code;
	private final LocalizedStringDTO name;

	public String getCode() {
		return code;
	}

	public LocalizedStringDTO getName() {
		return name;
	}

	private ModuleDetailCategoryDTO(Builder builder) {
		this.code = builder.code;
		this.name = builder.name;
	}

	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "with")
	public static class Builder {

		private String code;
		private LocalizedStringDTO name;

		public Builder withCode(String code) {
			this.code = code;
			return this;
		}

		public Builder withName(LocalizedStringDTO name) {
			this.name = name;
			return this;
		}

		public ModuleDetailCategoryDTO build() {
			return new ModuleDetailCategoryDTO(this);
		}

	}

	@Override
	public String toString() {
		return "ModuleDetailCategoryDTO [code=" + code + ", name=" + name + "]";
	}

}
