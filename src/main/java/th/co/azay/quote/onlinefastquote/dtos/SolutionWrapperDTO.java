package th.co.azay.quote.onlinefastquote.dtos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class SolutionWrapperDTO {

	private final SolutionDTO solution;
	private final Map<String, ModuleDetailDTO> moduleDetail;
	private final AgeDetailDTO ageDetail;
	private final OccupationDetailDTO[] occupations;
	private final BigDecimal totalPremiumAnnual;

	public SolutionDTO getSolution() {
		return solution;
	}

	public Map<String, ModuleDetailDTO> getModuleDetail() {
		return moduleDetail;
	}

	public AgeDetailDTO getAgeDetail() {
		return ageDetail;
	}

	public List<OccupationDetailDTO> getOccupations() {
		if (occupations == null) {
			return new ArrayList<>();
		}

		return Arrays.asList(occupations);
	}

	public BigDecimal getTotalPremiumAnnual() {
		return totalPremiumAnnual;
	}

	public SolutionWrapperDTO(Builder builder) {
		this.solution = builder.solution;
		this.moduleDetail = builder.moduleDetail;
		this.ageDetail = builder.ageDetail;
		this.occupations = builder.occupations;
		this.totalPremiumAnnual = builder.totalPremiumAnnual;
	}

	public static class Builder {

		private SolutionDTO solution;
		private Map<String, ModuleDetailDTO> moduleDetail;
		private AgeDetailDTO ageDetail;
		private OccupationDetailDTO[] occupations;
		private BigDecimal totalPremiumAnnual;

		public Builder withSolution(SolutionDTO solution) {
			this.solution = solution;
			return this;
		}

		public Builder withModuleDetail(Map<String, ModuleDetailDTO> moduleDetail) {
			this.moduleDetail = moduleDetail;
			return this;
		}

		public Builder withAgeDetail(AgeDetailDTO ageDetail) {
			this.ageDetail = ageDetail;
			return this;
		}

		public Builder withOccupations(List<OccupationDetailDTO> occupations) {
			this.occupations = occupations.toArray(new OccupationDetailDTO[occupations.size()]);
			return this;
		}

		public Builder withTotalPremiumAnnual(BigDecimal totalPremiumAnnual) {
			this.totalPremiumAnnual = totalPremiumAnnual;
			return this;
		}

		public SolutionWrapperDTO build() {
			return new SolutionWrapperDTO(this);
		}

	}

	@Override
	public String toString() {
		return "SolutionWrapperDTO [solution=" + solution + ", moduleDetail=" + moduleDetail + ", ageDetail=" + ageDetail + ", occupations="
				+ Arrays.toString(occupations) + ", totalPremiumAnnual=" + totalPremiumAnnual + "]";
	}

}
