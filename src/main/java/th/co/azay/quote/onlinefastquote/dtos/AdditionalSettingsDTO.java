package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class AdditionalSettingsDTO {

	private SalesIllustrationDTO salesIllustration;
	private NoteDTO note;
	private ReminderDTO reminder;

	@JsonInclude(Include.ALWAYS)
	public SalesIllustrationDTO getSalesIllustration() {
		return salesIllustration;
	}

	public void setSalesIllustration(SalesIllustrationDTO salesIllustration) {
		this.salesIllustration = salesIllustration;
	}

	@JsonInclude(Include.ALWAYS)
	public NoteDTO getNote() {
		return note;
	}

	public void setNote(NoteDTO note) {
		this.note = note;
	}

	@JsonInclude(Include.ALWAYS)
	public ReminderDTO getReminder() {
		return reminder;
	}

	public void setReminder(ReminderDTO reminder) {
		this.reminder = reminder;
	}

}
