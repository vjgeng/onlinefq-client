package th.co.azay.quote.onlinefastquote.dtos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = ModuleDetailDTO.Builder.class)
public class ModuleDetailDTO {

	private final String code;
	private final BigDecimal sumAssurance;
	private final BigDecimal[] sumAssurances;
	private final BigDecimal[] paymentPeriods;
	private final String status;
	private final String type;

	public String getCode() {
		return code;
	}

	public BigDecimal getSumAssurance() {
		return sumAssurance;
	}

	public List<BigDecimal> getSumAssurances() {
		if (sumAssurances == null) {
			return new ArrayList<>();
		}

		return Arrays.asList(sumAssurances);
	}

	public List<BigDecimal> getPaymentPeriods() {
		if (paymentPeriods == null) {
			return new ArrayList<>();
		}

		return Arrays.asList(paymentPeriods);
	}

	public String getStatus() {
		return status;
	}

	public String getType() {
		return type;
	}

	public ModuleDetailDTO(Builder builder) {
		this.code = builder.code;
		this.sumAssurance = builder.sumAssurance;
		this.sumAssurances = builder.sumAssurances;
		this.paymentPeriods = builder.paymentPeriods;
		this.status = builder.status;
		this.type = builder.type;
	}

	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "with")
	public static class Builder {

		private String code;
		private BigDecimal sumAssurance;
		private BigDecimal[] sumAssurances;
		private BigDecimal[] paymentPeriods;
		private String status;
		private String type;

		public Builder withCode(String code) {
			this.code = code;
			return this;
		}

		public Builder withSumAssurance(BigDecimal sumAssurance) {
			this.sumAssurance = sumAssurance;
			return this;
		}

		public Builder withSumAssurances(List<BigDecimal> sumAssurances) {
			this.sumAssurances = sumAssurances.toArray(new BigDecimal[sumAssurances.size()]);
			return this;
		}

		public Builder withPaymentPeriods(List<BigDecimal> paymentPeriods) {
			this.paymentPeriods = paymentPeriods.toArray(new BigDecimal[paymentPeriods.size()]);
			return this;
		}

		public Builder withStatus(String status) {
			this.status = status;
			return this;
		}

		public Builder withType(String type) {
			this.type = type;
			return this;
		}

		public ModuleDetailDTO build() {
			return new ModuleDetailDTO(this);
		}
	}

	@Override
	public String toString() {
		return "ModuleDetailDTO [code=" + code + ", sumAssurance=" + sumAssurance + ", sumAssurances=" + Arrays.toString(sumAssurances)
				+ ", paymentPeriods=" + Arrays.toString(paymentPeriods) + ", status=" + status + ", type=" + type + "]";
	}

}
