package th.co.azay.quote.onlinefastquote.dtos;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class UnderwritingRuleDTO {

	private List<RuleDTO> standaloneRules;
	private List<RuleDTO> accumulatedRules;

	@JsonInclude(Include.ALWAYS)
	public List<RuleDTO> getStandaloneRules() {
		return standaloneRules;
	}

	public void setStandaloneRules(List<RuleDTO> standaloneRules) {
		this.standaloneRules = standaloneRules;
	}

	@JsonInclude(Include.ALWAYS)
	public List<RuleDTO> getAccumulatedRules() {
		return accumulatedRules;
	}

	public void setAccumulatedRules(List<RuleDTO> accumulatedRules) {
		this.accumulatedRules = accumulatedRules;
	}

}
