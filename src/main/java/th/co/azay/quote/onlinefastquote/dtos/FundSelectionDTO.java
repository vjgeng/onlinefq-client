package th.co.azay.quote.onlinefastquote.dtos;

import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FundSelectionDTO {

	private String moduleCode;
	private Set<FundPercentageDTO> fundPercentages;

	@JsonInclude(Include.ALWAYS)
	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	@JsonInclude(Include.ALWAYS)
	public Set<FundPercentageDTO> getFundPercentages() {
		return fundPercentages;
	}

	public void setFundPercentages(Set<FundPercentageDTO> fundPercentages) {
		this.fundPercentages = fundPercentages;
	}

}
