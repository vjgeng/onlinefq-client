package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = MailDTO.Builder.class)
public class MailDTO {

	private final String to;
	private final String cc;
	private final String bcc;
	private final String subject;
	private final String body;
	private final String isHtml;

	public String getTo() {
		return to;
	}

	public String getCc() {
		return cc;
	}

	public String getSubject() {
		return subject;
	}

	public String getBcc() {
		return bcc;
	}

	public String getBody() {
		return body;
	}

	public String getIsHtml() {
		return isHtml;
	}

	public MailDTO(Builder builder) {
		this.to = builder.to;
		this.cc = builder.cc;
		this.bcc = builder.bcc;
		this.subject = builder.subject;
		this.body = builder.body;
		this.isHtml = builder.isHtml;
	}

	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "with")
	public static class Builder {

		private String to;
		private String cc;
		private String bcc;
		private String subject;
		private String body;
		private String isHtml;

		public Builder withTo(String to) {
			this.to = to;
			return this;
		}

		public Builder withCc(String cc) {
			this.cc = cc;
			return this;
		}

		public Builder withBcc(String bcc) {
			this.bcc = bcc;
			return this;
		}

		public Builder withSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public Builder withBody(String body) {
			this.body = body;
			return this;
		}

		public Builder withIsHtml(String isHtml) {
			this.isHtml = isHtml;
			return this;
		}

		public MailDTO build() {
			return new MailDTO(this);
		}
	}

	@Override
	public String toString() {
		return "MailDTO [to=" + to + ", cc=" + cc + ", bcc=" + bcc + ", subject=" + subject + ", body=" + body + ", isHtml=" + isHtml + "]";
	}

}
