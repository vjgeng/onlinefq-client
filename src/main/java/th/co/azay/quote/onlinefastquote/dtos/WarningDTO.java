package th.co.azay.quote.onlinefastquote.dtos;

public class WarningDTO {

	private SelectWarningDTO select;
	private DeselectWarningDTO deselect;

	public SelectWarningDTO getSelect() {
		return select;
	}

	public void setSelect(SelectWarningDTO select) {
		this.select = select;
	}

	public DeselectWarningDTO getDeselect() {
		return deselect;
	}

	public void setDeselect(DeselectWarningDTO deselect) {
		this.deselect = deselect;
	}

}
