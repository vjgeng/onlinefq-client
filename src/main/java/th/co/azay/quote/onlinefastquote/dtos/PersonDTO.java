package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.LocalDate;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonDTO {

	private String code;
	private String name;
	private LocalDate dateOfBirth;
	private Gender gender;
	private MaritalStatus maritalStatus;
	private String status;
	private int noOfChildren;
	private boolean smoker;
	private OccupationDetailDTO occupation;
	private Map<String, String> extendedProperties;

	@JsonInclude(Include.ALWAYS)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonInclude(Include.ALWAYS)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = CustomLocalDateSerializer.class)
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	@JsonDeserialize(using = CustomLocalDateDeserializer.class)
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@JsonInclude(Include.ALWAYS)
	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@JsonInclude(Include.ALWAYS)
	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	@JsonInclude(Include.ALWAYS)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonInclude(Include.ALWAYS)
	public int getNoOfChildren() {
		return noOfChildren;
	}

	public void setNoOfChildren(int noOfChildren) {
		this.noOfChildren = noOfChildren;
	}

	@JsonInclude(Include.ALWAYS)
	public boolean isSmoker() {
		return smoker;
	}

	public void setSmoker(boolean smoker) {
		this.smoker = smoker;
	}

	@JsonInclude(Include.ALWAYS)
	public OccupationDetailDTO getOccupation() {
		return occupation;
	}

	public void setOccupation(OccupationDetailDTO occupation) {
		this.occupation = occupation;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, String> getExtendedProperties() {
		return extendedProperties;
	}

	public void setExtendedProperties(Map<String, String> extendedProperties) {
		this.extendedProperties = extendedProperties;
	}

	@Override
	public String toString() {
		return "PersonDTO [code=" + code + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", gender=" + gender + ", maritalStatus="
				+ maritalStatus + ", status=" + status + ", noOfChildren=" + noOfChildren + ", smoker=" + smoker + ", occupation=" + occupation
				+ ", extendedProperties=" + extendedProperties + "]";
	}

}
