package th.co.azay.quote.onlinefastquote.dtos;

public enum MaritalStatus {
	SINGLE,
	MARRIED
}
