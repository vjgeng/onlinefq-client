package th.co.azay.quote.onlinefastquote.dtos;

public enum DataType {
	NUMBER, CHARACTER, INTEGER, DATE, TEXT, FORMULA, PERCENTAGE, OBJECT, BOOLEAN,FLOAT
}
