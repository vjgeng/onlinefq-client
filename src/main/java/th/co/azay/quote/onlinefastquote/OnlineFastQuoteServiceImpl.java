package th.co.azay.quote.onlinefastquote;


import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import th.co.azay.quote.onlinefastquote.dtos.SolutionDTO;
import th.co.azay.quote.onlinefastquote.dtos.SolutionWrapperDTO;
import org.springframework.stereotype.Service;
import th.co.azay.quote.onlinefastquote.dtos.findproduct.ProductDetail;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service("onlineFastQuoteService")
public class OnlineFastQuoteServiceImpl implements OnlineFastQuoteService {
    private static final Logger LOG = LoggerFactory.getLogger(OnlineFastQuoteServiceImpl.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    OnlineFastQuoteHandler onlineFastQuoteHandler;

    @Override
    public ProductDetail findProduct(String url, String productCode) throws CreateQuoteException {
        return this.findProductFQ(url, productCode);
    }

    @Override
    public SolutionDTO createSolution(String url, String channel, String oe, String form, String productCode) throws CreateQuoteException {
        return this.createSolutionFQ(url, channel, oe, form, productCode);
    }

    @Override
    public SolutionWrapperDTO calculatePremium(String url, String channel, String oe, String form, SolutionDTO solution) throws CreateQuoteException {
        return this.calculatePremiumFQ(url, channel, oe, form, solution);
    }

    public ProductDetail findProductFQ(String url, String productCode) throws CreateQuoteException {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("productCode", productCode);


        List list = new ArrayList();

        HttpHeaders headers = this.setHeaders();
        HttpEntity<List> request = new HttpEntity<List>(list, headers);

        //ResponseEntity<String> response = restTemplate.getForEntity(builder.build().toString(), String.class, request);
        //ProductDetail product = this.handleResponse(response.getBody(), ProductDetail.class);

        ResponseEntity<Resource> response = restTemplate.getForEntity(builder.build().toString(), Resource.class, request);
        InputStream responseInputStream = null;
        ProductDetail product = null;
        try {
            responseInputStream = response.getBody().getInputStream();
            product = onlineFastQuoteHandler.handleResponse( "datas", responseInputStream, ProductDetail.class);
        } catch (IOException ioe) {
            throw new CreateQuoteException(ioe.getMessage());
        }



        return product;
    }

    public SolutionDTO createSolutionFQ(String url, String channel, String oe, String form, String productCode) throws CreateQuoteException{
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("channel", channel)
                .queryParam("oe", oe)
                .queryParam("form", form);


        List list = new ArrayList();
        list.add(productCode);

        HttpHeaders headers = this.setHeaders();
        HttpEntity<List> request = new HttpEntity<List>(list, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(builder.build().toUri(), request, String.class);
        SolutionDTO solution = null;

        try {
            solution = onlineFastQuoteHandler.handleResponseCreateSolution(response.getBody(), SolutionDTO.class);
        }catch (IOException ioe){
            throw new CreateQuoteException(ioe.getMessage());
        }

        return solution;

        /*
        ResponseEntity<Resource> response = restTemplate.postForEntity(builder.build().toUri(), request, Resource.class);
        InputStream responseInputStream = null;
        SolutionDTO solution = null;
        try {
            responseInputStream = response.getBody().getInputStream();
            solution = handleResponse( "datas.solution", responseInputStream, SolutionDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return solution;
        */
    }

    public SolutionWrapperDTO calculatePremiumFQ(String url, String channel, String oe, String form, SolutionDTO solution) throws CreateQuoteException{
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("channel", channel)
                .queryParam("oe", oe)
                .queryParam("form", form);


        HttpHeaders headers = this.setHeaders();
        HttpEntity<SolutionDTO> request = new HttpEntity<SolutionDTO>(solution, headers);

        Resource response = restTemplate.postForObject(builder.build().toUri(), request, Resource.class);
        SolutionWrapperDTO baseSolution = null;
        //SolutionDTO solutionResp = null;
        //BigDecimal totalPremiumAnnual = null;
        InputStream responseInputStream = null;

        try {
            responseInputStream = response.getInputStream();
            //solutionResp = this.handleResponseCalculatePremium(responseInputStream, SolutionDTO.class);
            //totalPremiumAnnual = this.getTotalPremium(responseInputStream, BigDecimal.class);

            //baseSolution = new SolutionWrapperDTO.Builder().withSolution(solutionResp).withTotalPremiumAnnual(totalPremiumAnnual).build();
            baseSolution = onlineFastQuoteHandler.getBaseSolution(responseInputStream);
        }catch (IOException ioe){
            throw new CreateQuoteException(ioe.getMessage());
        }

        return baseSolution;
    }

    private HttpHeaders setHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Origin", "");
        headers.set("Accept-Encoding", "gzip, deflate, br");
        headers.set("Accept-Language", "en-US,en;q=0.9,th;q=0.8");
        headers.set("User-Agent", "Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1");
        headers.set("Content-Type", "application/json;charset=UTF-8");
        headers.set("Accept", "application/json, text/plain, */*");
        headers.set("Referer", "");
        headers.set("Cookie", "WebSessionID=194.127.91.17.1536133252939809; _ga=GA1.3.1900006223.1536566260; _gid=GA1.3.1465568434.1536566260; _gat_UA-84267324-1=1; _hjIncludedInSample=1; __zlcmid=oKhdD5OEcVoO55");
        headers.set("Connection", "keep-alive");

        return headers;
    }

    @Bean
    public RestTemplate restTemplate() {
        final String username = "";
        final String password = "";
        final String proxyUrl = "";
        final int port = 80;

        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(new AuthScope(proxyUrl, port),  new UsernamePasswordCredentials(username, password));

        HttpHost myProxy = new HttpHost(proxyUrl, port);
        HttpClientBuilder clientBuilder = HttpClientBuilder.create();

        clientBuilder.setProxy(myProxy).setDefaultCredentialsProvider(credsProvider).disableCookieManagement();

        HttpClient httpClient = clientBuilder.build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setHttpClient(httpClient);
        factory.setReadTimeout(3000000);
        factory.setConnectTimeout(3000000);
        factory.setConnectionRequestTimeout(3000000);

        return new RestTemplate(factory);
    }



}
