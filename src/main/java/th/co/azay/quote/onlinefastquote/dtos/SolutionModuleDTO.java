package th.co.azay.quote.onlinefastquote.dtos;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolutionModuleDTO {

	private String code;
	private String version;
	private String type;
	private String benefitType;
	private String need;
	private String plan;
	private String insuredPerson;

	private Map<String, RangeDTO> ranges;
	private Map<String, Object[]> selections;

	private Map<String, Object> options;
	private Map<String, Object> outputs;
	private String linkedTo;
	private Map<String, String> extendedProperties;

	private boolean selected;
	private WarningDTO warning;

	@JsonInclude(Include.ALWAYS)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonInclude(Include.ALWAYS)
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@JsonInclude(Include.ALWAYS)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonInclude(Include.ALWAYS)
	public String getBenefitType() {
		return benefitType;
	}

	public void setBenefitType(String benefitType) {
		this.benefitType = benefitType;
	}

	@JsonInclude(Include.ALWAYS)
	public String getNeed() {
		return need;
	}

	public void setNeed(String need) {
		this.need = need;
	}

	@JsonInclude(Include.ALWAYS)
	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	@JsonInclude(Include.ALWAYS)
	public String getInsuredPerson() {
		return insuredPerson;
	}

	public void setInsuredPerson(String insuredPerson) {
		this.insuredPerson = insuredPerson;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, RangeDTO> getRanges() {
		return ranges;
	}

	public void setRanges(Map<String, RangeDTO> ranges) {
		this.ranges = ranges;
	}

	public Map<String, Object[]> getSelections() {
		return selections;
	}

	public void setSelections(Map<String, Object[]> selections) {
		this.selections = selections;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, Object> getOptions() {
		return options;
	}

	public void setOptions(Map<String, Object> options) {
		this.options = options;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, Object> getOutputs() {
		return outputs;
	}

	public void setOutputs(Map<String, Object> outputs) {
		this.outputs = outputs;
	}

	@JsonInclude(Include.ALWAYS)
	public String getLinkedTo() {
		return linkedTo;
	}

	public void setLinkedTo(String linkedTo) {
		this.linkedTo = linkedTo;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, String> getExtendedProperties() {
		return extendedProperties;
	}

	public void setExtendedProperties(Map<String, String> extendedProperties) {
		this.extendedProperties = extendedProperties;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public WarningDTO getWarning() {
		return warning;
	}

	public static SolutionModuleDTO immutableSolutionModuleDTOFactory(SolutionModuleDTO source) {
		SolutionModuleDTO solutionModuleDTO = new SolutionModuleDTO();
		solutionModuleDTO.setSelected(source.isSelected());
		solutionModuleDTO.setRanges(new HashMap<>(source.getRanges()));
		solutionModuleDTO.setType(source.getType());
		solutionModuleDTO.setPlan(source.getPlan());
		solutionModuleDTO.setBenefitType(source.getBenefitType());
		solutionModuleDTO.setCode(source.getCode());
		solutionModuleDTO.setExtendedProperties(new HashMap<>(source.getExtendedProperties()));
		solutionModuleDTO.setInsuredPerson(source.getInsuredPerson());
		solutionModuleDTO.setLinkedTo(source.getLinkedTo());
		solutionModuleDTO.setNeed(source.getNeed());
		solutionModuleDTO.setOptions(new HashMap<>(source.getOptions()));
		solutionModuleDTO.setOutputs(new HashMap<>(source.getOutputs()));
		solutionModuleDTO.setRanges(new HashMap<>(source.getRanges()));
		solutionModuleDTO.setSelections(new HashMap<>(source.getSelections()));
		solutionModuleDTO.setVersion(source.getVersion());
		solutionModuleDTO.setWarning(source.getWarning());
		return solutionModuleDTO;
	}
	public void setWarning(WarningDTO warning) {
		this.warning = warning;
	}

	@Override
	public String toString() {
		return "SolutionModuleDTO [code=" + code + ", version=" + version + ", type=" + type + ", benefitType=" + benefitType + ", need=" + need
				+ ", plan=" + plan + ", insuredPerson=" + insuredPerson + ", ranges=" + ranges + ", options=" + options + ", outputs=" + outputs
				+ ", linkedTo=" + linkedTo + ", extendedProperties=" + extendedProperties + ", selected=" + selected + "]";
	}

}
