package th.co.azay.quote.onlinefastquote.dtos;

public class DeselectWarningDTO {

	private String[] children;
	private String[] bundles;

	public String[] getChildren() {
		return children;
	}

	public void setChildren(String[] children) {
		this.children = children;
	}

	public String[] getBundles() {
		return bundles;
	}

	public void setBundles(String[] bundles) {
		this.bundles = bundles;
	}

}
