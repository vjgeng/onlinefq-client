package th.co.azay.quote.onlinefastquote.dtos;

public enum SolutionStatus {

	RECOMMENDED,
	SAVED,
	DELETED

}
