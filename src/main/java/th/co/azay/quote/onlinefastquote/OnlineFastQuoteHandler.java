package th.co.azay.quote.onlinefastquote;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import th.co.azay.quote.onlinefastquote.dtos.SolutionDTO;
import th.co.azay.quote.onlinefastquote.dtos.SolutionWrapperDTO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

@Service("onlineFastQuoteHandler")
public class OnlineFastQuoteHandler {
    /*
    public <T> T handleResponse(String response, Class<T> type) throws CreateQuoteException {
        Object obj = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode rootNode = mapper.readTree(response);
            JsonNode dataNode = rootNode.path("datas");

            obj = mapper.readValue(dataNode.toString(), type);
        }catch (IOException e){
            e.printStackTrace();
            throw new CreateQuoteException();
        }

        return type.cast(obj);
    }
    */

    public synchronized <T> T handleResponse(String jsonPath, InputStream ins, Class<T> type) throws CreateQuoteException, IOException {
        FileOutputStream fout = null;
        Object obj = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode rootNode = mapper.readTree(ins);
            JsonNode dataNode = rootNode.path(jsonPath);

            File f = new File("./response.json");
            fout = new FileOutputStream(f);
            fout.write(dataNode.toString().getBytes("UTF-8"));
            fout.close();

            obj = mapper.readValue(dataNode.toString(), type);
        }catch (IOException e){
            e.printStackTrace();
            throw new CreateQuoteException(e.getMessage());
        } finally{
            fout.close();
        }

        return type.cast(obj);
    }


    public synchronized <T> T handleResponseCreateSolution(String response, Class<T> type) throws CreateQuoteException, IOException {
        Object obj = null;
        ObjectMapper mapper = new ObjectMapper();

        JsonNode rootNode = mapper.readTree(response);
        JsonNode dataNode = rootNode.path("datas");
        JsonNode solutionNode = dataNode.path("solution");

        obj = mapper.readValue(this.writeFileJson(solutionNode), type);


        return type.cast(obj);
    }

    /*
    public <T> T handleResponseCalculatePremium(InputStream ins, Class<T> type) throws CreateQuoteException, IOException {
        Object obj = null;
        ObjectMapper mapper = new ObjectMapper();
        FileOutputStream fout = null;
        try {
            JsonNode rootNode = mapper.readTree(ins);
            JsonNode dataNode = rootNode.path("datas");
            //JsonNode baseSolutionNode = dataNode.path("baseSolution");
            //JsonNode solutionNode = baseSolutionNode.path("solution");
            JsonNode solutionNode = dataNode.path("baseSolution.solution");

            File f = new File("c:/response.json");
            fout = new FileOutputStream(f);
            fout.write(solutionNode.toString().getBytes("UTF-8"));
            fout.close();

            obj = mapper.readValue(new File("c:/response.json"), type);
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            fout.close();
        }

        return type.cast(obj);
    }
    */

    public synchronized SolutionWrapperDTO getBaseSolution(InputStream ins) throws CreateQuoteException, IOException {
        SolutionWrapperDTO baseSolution = null;
        SolutionDTO solution = null;
        BigDecimal totalPremiumAnnual = null;
        ObjectMapper mapper = new ObjectMapper();
        Object obj = null;
        //FileOutputStream fout = null;

        try {
            JsonNode rootNode = mapper.readTree(ins);
            JsonNode dataNode = rootNode.path("datas");
            JsonNode baseSolutionNode = dataNode.path("baseSolution");
            JsonNode solutionNode = baseSolutionNode.path("solution");
            JsonNode totalPremiumAnnualNode = baseSolutionNode.path("totalPremiumAnnual");

            solution = mapper.readValue(this.writeFileJson(solutionNode), SolutionDTO.class);
            totalPremiumAnnual = mapper.readValue(this.writeFileJson(totalPremiumAnnualNode), BigDecimal.class);

            baseSolution = new SolutionWrapperDTO.Builder().withSolution(solution).withTotalPremiumAnnual(totalPremiumAnnual).build();
        }catch (Exception e){
            throw new CreateQuoteException(e.getMessage());
        }

        return baseSolution;
    }

    private synchronized File writeFileJson(JsonNode jsonNode) throws IOException {
        FileOutputStream fout = null;
        File f = new File("./response.json");

        try {
            fout = new FileOutputStream(f);
            fout.write(jsonNode.toString().getBytes("UTF-8"));
            fout.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            fout.close();
        }

        return f;
    }
}
