package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SelectWarningDTO {

	private RangeDTO age;
	private String[] occRate;
	private String[] status;
	private String[] gender;
	private boolean[] smoker;
	private String[] conflicts;
	private String[] dependencies;

	public RangeDTO getAge() {
		return age;
	}

	public void setAge(RangeDTO age) {
		this.age = age;
	}

	public String[] getOccRate() {
		return occRate;
	}

	public void setOccRate(String[] occRate) {
		this.occRate = occRate;
	}

	public String[] getStatus() {
		return status;
	}

	public void setStatus(String[] status) {
		this.status = status;
	}

	public String[] getGender() {
		return gender;
	}

	public void setGender(String[] gender) {
		this.gender = gender;
	}

	public boolean[] getSmoker() {
		return smoker;
	}

	public void setSmoker(boolean[] smoker) {
		this.smoker = smoker;
	}

	public String[] getConflicts() {
		return conflicts;
	}

	public void setConflicts(String[] conflicts) {
		this.conflicts = conflicts;
	}

	public String[] getDependencies() {
		return dependencies;
	}

	public void setDependencies(String[] dependencies) {
		this.dependencies = dependencies;
	}

}
