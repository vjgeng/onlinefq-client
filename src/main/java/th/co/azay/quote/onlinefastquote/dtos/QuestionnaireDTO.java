package th.co.azay.quote.onlinefastquote.dtos;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

@Data
public class QuestionnaireDTO {
    private String code;
    private boolean enabled;
    private boolean display;
    private List<ProductRules> rules;
    // private List<RiderDetails> riderDetails;
    // private PersonalDetail personalDetail;

    @Data
    public static class ProductRules {
        private String gender;
        private int minAge;
        private int maxAge;
        private String occupation;
        private List<RiderDetails> riderDetails;
        private List<RiderDetails> additionalRiderDetails;
    }

    // @Data
    // public static class PersonalDetail {
    //     private Gender gender;
    //     private int age;
    //     private String occupation;
    // }

    @Data
    public static class RiderDetails {
        private String code;
        private BigDecimal sumAssured;
        private List<RiderDetailsOptions> options;
    }

    @Data
    public static class RiderDetailsOptions {
        private String key;
        private String value;
        private String type;
    }
}
