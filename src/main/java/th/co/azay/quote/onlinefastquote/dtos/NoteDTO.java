package th.co.azay.quote.onlinefastquote.dtos;

import java.time.ZonedDateTime;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NoteDTO {

	private String content;
	private ZonedDateTime lastModifiedDate;

	@JsonInclude(Include.ALWAYS)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	public ZonedDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
