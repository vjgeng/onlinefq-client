package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OutputDTO {

	private String code;
	private DataType outputType;
	private String formulaCode;

	@JsonIgnore
	private FormulaDTO formula;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public DataType getOutputType() {
		return outputType;
	}

	public void setOutputType(DataType outputType) {
		this.outputType = outputType;
	}

	public String getFormulaCode() {
		return formulaCode;
	}

	public void setFormulaCode(String formulaCode) {
		this.formulaCode = formulaCode;
	}

	public FormulaDTO getFormula() {
		return formula;
	}

	public void setFormula(FormulaDTO formula) {
		this.formula = formula;
	}

}
