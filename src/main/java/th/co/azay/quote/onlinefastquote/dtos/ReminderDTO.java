package th.co.azay.quote.onlinefastquote.dtos;

import java.time.ZonedDateTime;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReminderDTO {

	private String code;
	private String subject;
	private ZonedDateTime dueDate;
	private Long alertTime;

	@JsonInclude(Include.ALWAYS)
	@JsonProperty("systemId")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonInclude(Include.ALWAYS)
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	public ZonedDateTime getDueDate() {
		return dueDate;
	}

	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	public void setDueDate(ZonedDateTime dueDate) {
		this.dueDate = dueDate;
	}

	@JsonInclude(Include.ALWAYS)
	public Long getAlertTime() {
		return alertTime;
	}

	public void setAlertTime(Long alertTime) {
		this.alertTime = alertTime;
	}
}
