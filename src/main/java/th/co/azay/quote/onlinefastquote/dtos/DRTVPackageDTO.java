package th.co.azay.quote.onlinefastquote.dtos;

import java.math.BigDecimal;

public class DRTVPackageDTO {
    private static final long serialVersionUID = 1L;

    private String packageCode;
    private int age;
    private Gender gender;
    private PaymentFrequency paymentFrequency;
    private String planNo1;
    private BigDecimal premium1;
    private BigDecimal sumAssured1;
    private String planNo2;
    private BigDecimal premium2;
    private BigDecimal sumAssured2;
    private String planNo3;
    private BigDecimal premium3;
    private BigDecimal sumAssured3;


    public DRTVPackageDTO() {
        // Required to initialize by Spring JPA.
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public PaymentFrequency getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(PaymentFrequency paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public BigDecimal getPremium1() {
        return premium1;
    }

    public void setPremium1(BigDecimal premium1) {
        this.premium1 = premium1;
    }

    public BigDecimal getSumAssured1() {
        return sumAssured1;
    }

    public void setSumAssured1(BigDecimal sumAssured1) {
        this.sumAssured1 = sumAssured1;
    }

    public BigDecimal getPremium2() {
        return premium2;
    }

    public void setPremium2(BigDecimal premium2) {
        this.premium2 = premium2;
    }

    public BigDecimal getSumAssured2() {
        return sumAssured2;
    }

    public void setSumAssured2(BigDecimal sumAssured2) {
        this.sumAssured2 = sumAssured2;
    }

    public BigDecimal getPremium3() {
        return premium3;
    }

    public void setPremium3(BigDecimal premium3) {
        this.premium3 = premium3;
    }

    public BigDecimal getSumAssured3() {
        return sumAssured3;
    }

    public void setSumAssured3(BigDecimal sumAssured3) {
        this.sumAssured3 = sumAssured3;
    }

    public String getPlanNo1() {
        return planNo1;
    }

    public void setPlanNo1(String planNo1) {
        this.planNo1 = planNo1;
    }

    public String getPlanNo2() {
        return planNo2;
    }

    public void setPlanNo2(String planNo2) {
        this.planNo2 = planNo2;
    }

    public String getPlanNo3() {
        return planNo3;
    }

    public void setPlanNo3(String planNo3) {
        this.planNo3 = planNo3;
    }

    @Override
    public String toString() {
        return "DRTVPackageDTO{" +
                "packageCode='" + packageCode + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", paymentFrequency=" + paymentFrequency +
                ", planNo1='" + planNo1 + '\'' +
                ", premium1=" + premium1 +
                ", sumAssured1=" + sumAssured1 +
                ", planNo2='" + planNo2 + '\'' +
                ", premium2=" + premium2 +
                ", sumAssured2=" + sumAssured2 +
                ", planNo3='" + planNo3 + '\'' +
                ", premium3=" + premium3 +
                ", sumAssured3=" + sumAssured3 +
                '}';
    }

    public static final class Builder {
        private String packageCode;
        private int age;
        private Gender gender;
        private PaymentFrequency paymentFrequency;
        private String planNo1;
        private BigDecimal premium1;
        private BigDecimal sumAssured1;
        private String planNo2;
        private BigDecimal premium2;
        private BigDecimal sumAssured2;
        private String planNo3;
        private BigDecimal premium3;
        private BigDecimal sumAssured3;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder packageCode(String packageCode) {
            this.packageCode = packageCode;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder gender(Gender gender) {
            this.gender = gender;
            return this;
        }

        public Builder paymentFrequency(PaymentFrequency paymentFrequency) {
            this.paymentFrequency = paymentFrequency;
            return this;
        }

        public Builder planNo1(String planNo1) {
            this.planNo1 = planNo1;
            return this;
        }

        public Builder premium1(BigDecimal premium1) {
            this.premium1 = premium1;
            return this;
        }

        public Builder sumAssured1(BigDecimal sumAssured1) {
            this.sumAssured1 = sumAssured1;
            return this;
        }

        public Builder planNo2(String planNo2) {
            this.planNo2 = planNo2;
            return this;
        }

        public Builder premium2(BigDecimal premium2) {
            this.premium2 = premium2;
            return this;
        }

        public Builder sumAssured2(BigDecimal sumAssured2) {
            this.sumAssured2 = sumAssured2;
            return this;
        }

        public Builder planNo3(String planNo3) {
            this.planNo3 = planNo3;
            return this;
        }

        public Builder premium3(BigDecimal premium3) {
            this.premium3 = premium3;
            return this;
        }

        public Builder sumAssured3(BigDecimal sumAssured3) {
            this.sumAssured3 = sumAssured3;
            return this;
        }

        public DRTVPackageDTO build() {
            DRTVPackageDTO drtvPackageDTO = new DRTVPackageDTO();
            drtvPackageDTO.setPackageCode(packageCode);
            drtvPackageDTO.setAge(age);
            drtvPackageDTO.setGender(gender);
            drtvPackageDTO.setPaymentFrequency(paymentFrequency);
            drtvPackageDTO.setPlanNo1(planNo1);
            drtvPackageDTO.setPremium1(premium1);
            drtvPackageDTO.setSumAssured1(sumAssured1);
            drtvPackageDTO.setPlanNo2(planNo2);
            drtvPackageDTO.setPremium2(premium2);
            drtvPackageDTO.setSumAssured2(sumAssured2);
            drtvPackageDTO.setPlanNo3(planNo3);
            drtvPackageDTO.setPremium3(premium3);
            drtvPackageDTO.setSumAssured3(sumAssured3);
            return drtvPackageDTO;
        }
    }
}
