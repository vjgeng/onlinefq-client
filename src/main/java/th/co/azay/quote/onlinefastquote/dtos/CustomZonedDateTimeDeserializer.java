package th.co.azay.quote.onlinefastquote.dtos;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class CustomZonedDateTimeDeserializer extends JsonDeserializer<ZonedDateTime> {

	@Override
	public ZonedDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException {
		LocalDateTime localDateTime = LocalDateTime.parse(parser.getText(), CustomZonedDateTimeSerializer.formatter);
		return ZonedDateTime.of(localDateTime, ZoneOffset.UTC);
	}

}
