package th.co.azay.quote.onlinefastquote.dtos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = CommonResponseDTO.Builder.class)
public class CommonResponseDTO<T> {

	private final Integer code;
	private final T datas;
	private final ErrorDTO[] errors;

	public Integer getCode() {
		return code;
	}

	public T getDatas() {
		return datas;
	}

	public List<ErrorDTO> getErrors() {
		if (errors == null) {
			return new ArrayList<>();
		}

		return Arrays.asList(errors);
	}

	public CommonResponseDTO(Builder<T> builder) {
		this.code = builder.code;
		this.datas = builder.datas;
		this.errors = builder.errors;
	}

	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "with")
	public static class Builder<T> {

		private Integer code;
		private T datas;
		private ErrorDTO[] errors;

		public Builder<T> withCode(Integer code) {
			this.code = code;
			return this;
		}

		public Builder<T> withDatas(T datas) {
			this.datas = datas;
			return this;
		}

		public Builder<T> withErrors(List<ErrorDTO> errors) {
			this.errors = errors.toArray(new ErrorDTO[errors.size()]);
			return this;
		}

		public CommonResponseDTO<T> build() {
			return new CommonResponseDTO<T>(this);
		}

	}

	@Override
	public String toString() {
		return "CommonResponseDTO [code=" + code + ", datas=" + datas + ", errors=" + Arrays.toString(errors) + "]";
	}

}
