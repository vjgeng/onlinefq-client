package th.co.azay.quote.onlinefastquote.dtos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder(builderClassName = "Builder")
public class OccupationDetailDTO {
    private Integer id;
    private String code;
    private LocalizedStringDTO name;
    private String riskLevel;
    private LocalizedStringDTO displayName;
    private List<OccupationDetailDTO> subCategories;

}

