package th.co.azay.quote.onlinefastquote.dtos;

import java.util.Map;

public class ModuleCache {

	private Map<String, Object> last;

	public Map<String, Object> getLast() {
		return last;
	}

	public void setLast(Map<String, Object> last) {
		this.last = last;
	}

}
