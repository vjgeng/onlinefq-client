package th.co.azay.quote.onlinefastquote.dtos;

import java.util.Map;

public class RuleDTO {

	private String code;
	private String rule;
	private Map<String, String> description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public Map<String, String> getDescription() {
		return description;
	}

	public void setDescription(Map<String, String> description) {
		this.description = description;
	}

}
