package th.co.azay.quote.onlinefastquote.dtos;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CustomZonedDateTimeSerializer extends JsonSerializer<ZonedDateTime> {

	static final DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;

	@Override
	public void serialize(ZonedDateTime zoneDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
		ZonedDateTime utcTime = zoneDateTime.withZoneSameInstant(ZoneOffset.UTC);
		jsonGenerator.writeString(utcTime.format(formatter));
	}
}
