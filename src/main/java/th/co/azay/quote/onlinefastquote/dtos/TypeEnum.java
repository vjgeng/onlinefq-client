package th.co.azay.quote.onlinefastquote.dtos;

public enum TypeEnum {

	BASEPLAN("BASEPLAN"), RIDER("RIDER");

	private final String value;

	TypeEnum(final String v) {
		this.value = v;
	}

	public String value() {
		return this.value;
	}

	public static TypeEnum fromValue(final String v) {

		for (final TypeEnum c : TypeEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(String.valueOf(v));
	}

}
