package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DRTVPackageJsonDTO {
    private String productCode;
    private Integer age;
    private Gender gender;
    private BigDecimal[] sumAssurances;
    private BigDecimal defaultSumAssurance;
    private BigDecimal[] premium;
    private BigDecimal defaultPremium;

    public String getProductCode() {
        return productCode;
    }

    public BigDecimal[] getSumAssurances() {
        return Arrays.copyOf(sumAssurances, sumAssurances.length);
    }

    public Gender getGender() {
        return gender;
    }

    public Integer getAge() {
        return age;
    }

    public BigDecimal[] getPremium() {
        return Arrays.copyOf(premium, premium.length);
    }

    public BigDecimal getDefaultPremium() {
        return defaultPremium;
    }

    public BigDecimal getDefaultSumAssurance() {
        return defaultSumAssurance;
    }

    public static final class Builder {
        private String productCode;
        private Integer age;
        private BigDecimal[] sumAssurances;
        private BigDecimal defaultSumAssurance;
        private Gender gender;
        private BigDecimal[] premium;
        private BigDecimal defaultPremium;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder withProductCode(String productCode) {
            this.productCode = productCode;
            return this;
        }

        public Builder withAge(Integer age) {
            this.age = age;
            return this;
        }

        public Builder withSumAssurances(BigDecimal[] sumAssurances) {
            this.sumAssurances = sumAssurances;
            return this;
        }

        public Builder withDefaultSumAssurance(BigDecimal defaultSumAssurance) {
            this.defaultSumAssurance = defaultSumAssurance;
            return this;
        }

        public Builder withGender(Gender gender) {
            this.gender = gender;
            return this;
        }

        public Builder withPremium(BigDecimal[] premium) {
            this.premium = premium;
            return this;
        }

        public Builder withDefaultPremium(BigDecimal defaultPremium) {
            this.defaultPremium = defaultPremium;
            return this;
        }

        public DRTVPackageJsonDTO build() {
            DRTVPackageJsonDTO dRTVPackageJsonDTO = new DRTVPackageJsonDTO();
            dRTVPackageJsonDTO.age = this.age;
            dRTVPackageJsonDTO.gender = this.gender;
            dRTVPackageJsonDTO.productCode = this.productCode;
            dRTVPackageJsonDTO.defaultPremium = this.defaultPremium;
            dRTVPackageJsonDTO.premium = this.premium;
            dRTVPackageJsonDTO.sumAssurances = this.sumAssurances;
            dRTVPackageJsonDTO.defaultSumAssurance = this.defaultSumAssurance;
            return dRTVPackageJsonDTO;
        }
    }
}
