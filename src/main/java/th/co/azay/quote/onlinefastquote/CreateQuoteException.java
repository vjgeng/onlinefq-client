package th.co.azay.quote.onlinefastquote;

public class CreateQuoteException extends Exception {
    public CreateQuoteException(String s) {
        super(s);
    }
}

