package th.co.azay.quote.onlinefastquote.dtos;

public enum PaymentFrequency {
	MONTHLY,
	QUARTERLY,
	SEMIANNUAL,
	ANNUAL
}
