
package th.co.azay.quote.onlinefastquote.dtos.findproduct;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "sumAssured",
    "options",
    "exactRiderCode"
})
public class AdditionalRiderDetail {

    @JsonProperty("code")
    private String code;
    @JsonProperty("sumAssured")
    private Integer sumAssured;
    @JsonProperty("options")
    private List<Object> options = null;
    @JsonProperty("exactRiderCode")
    private String exactRiderCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("sumAssured")
    public Integer getSumAssured() {
        return sumAssured;
    }

    @JsonProperty("sumAssured")
    public void setSumAssured(Integer sumAssured) {
        this.sumAssured = sumAssured;
    }

    @JsonProperty("options")
    public List<Object> getOptions() {
        return options;
    }

    @JsonProperty("options")
    public void setOptions(List<Object> options) {
        this.options = options;
    }

    @JsonProperty("exactRiderCode")
    public String getExactRiderCode() {
        return exactRiderCode;
    }

    @JsonProperty("exactRiderCode")
    public void setExactRiderCode(String exactRiderCode) {
        this.exactRiderCode = exactRiderCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
