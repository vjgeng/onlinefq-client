package th.co.azay.quote.onlinefastquote;

import org.springframework.stereotype.Service;
import th.co.azay.quote.onlinefastquote.dtos.SolutionDTO;
import th.co.azay.quote.onlinefastquote.dtos.SolutionWrapperDTO;
import th.co.azay.quote.onlinefastquote.dtos.findproduct.ProductDetail;

public interface OnlineFastQuoteService {
    ProductDetail findProduct(String url, String productCode) throws CreateQuoteException;
    SolutionDTO createSolution(String url, String channel, String oe, String form, String productCode) throws CreateQuoteException;
    SolutionWrapperDTO calculatePremium(String url, String channel, String oe, String form, SolutionDTO solution) throws CreateQuoteException;
}
