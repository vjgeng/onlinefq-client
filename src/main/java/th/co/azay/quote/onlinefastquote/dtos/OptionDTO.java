package th.co.azay.quote.onlinefastquote.dtos;

public class OptionDTO {

	private String code;
	private String defaultValue;
	private String formulaCode;
	private String interval;
	private DataType inputType;
	private LocalizedStringDTO name = new LocalizedStringDTO();
	private LocalizedStringDTO description = new LocalizedStringDTO();
	private Object options;
	private Boolean showOnTestPage;
	private Object ranges;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getFormulaCode() {
		return formulaCode;
	}

	public void setFormulaCode(String formulaCode) {
		this.formulaCode = formulaCode;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public Object getOptions() {
		return options;
	}

	public void setOptions(Object options) {
		this.options = options;
	}

	public LocalizedStringDTO getName() {
		return name;
	}

	public void setName(LocalizedStringDTO name) {
		this.name = name;
	}

	public LocalizedStringDTO getDescription() {
		return description;
	}

	public void setDescription(LocalizedStringDTO description) {
		this.description = description;
	}

	public DataType getInputType() {
		return inputType;
	}

	public void setInputType(DataType inputType) {
		this.inputType = inputType;
	}

	public Boolean getShowOnTestPage() {
		return showOnTestPage;
	}

	public void setShowOnTestPage(Boolean showOnTestPage) {
		this.showOnTestPage = showOnTestPage;
	}

	public Object getRanges() {
		return ranges;
	}

	public void setRanges(Object ranges) {
		this.ranges = ranges;
	}

}
