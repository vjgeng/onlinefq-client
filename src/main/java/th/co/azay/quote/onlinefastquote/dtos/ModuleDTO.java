package th.co.azay.quote.onlinefastquote.dtos;

import java.time.ZonedDateTime;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ModuleDTO {

	private String code;

	private String version;

	private ZonedDateTime effectiveDate;

	private ZonedDateTime endDate;

	private String category;

	private String type;

	private Integer priority;

	private String benefitType;

	private LocalizedStringDTO name = new LocalizedStringDTO();

	private LocalizedStringDTO description = new LocalizedStringDTO();

	private LocalizedStringDTO benefitPeriod = new LocalizedStringDTO();

	private List<OptionDTO> options;

	private List<OutputDTO> outputs;

	private List<FormulaDTO> formulas;

	private UnderwritingRuleDTO underwritingRules;

	@JsonInclude(Include.ALWAYS)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonInclude(Include.ALWAYS)
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	public ZonedDateTime getEffectiveDate() {
		return effectiveDate;
	}

	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	public void setEffectiveDate(ZonedDateTime effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	public ZonedDateTime getEndDate() {
		return endDate;
	}

	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	@JsonInclude(Include.ALWAYS)
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@JsonInclude(Include.ALWAYS)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonInclude(Include.ALWAYS)
	public LocalizedStringDTO getName() {
		return name;
	}

	public void setName(LocalizedStringDTO name) {
		this.name = name;
	}

	@JsonInclude(Include.ALWAYS)
	public List<OptionDTO> getOptions() {
		return options;
	}

	public void setOptions(List<OptionDTO> options) {
		this.options = options;
	}

	@JsonInclude(Include.ALWAYS)
	public List<OutputDTO> getOutputs() {
		return outputs;
	}

	public void setOutputs(List<OutputDTO> outputs) {
		this.outputs = outputs;
	}

	@JsonInclude(Include.ALWAYS)
	public List<FormulaDTO> getFormulas() {
		return formulas;
	}

	public void setFormulas(List<FormulaDTO> formulas) {
		this.formulas = formulas;
	}

	@JsonInclude(Include.ALWAYS)
	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	@JsonInclude(Include.ALWAYS)
	public UnderwritingRuleDTO getUnderwritingRules() {
		return underwritingRules;
	}

	public void setUnderwritingRules(UnderwritingRuleDTO underwritingRule) {
		this.underwritingRules = underwritingRule;
	}

	public String getBenefitType() {
		return benefitType;
	}

	public void setBenefitType(String benefitType) {
		this.benefitType = benefitType;
	}

	public LocalizedStringDTO getDescription() {
		return description;
	}

	public void setDescription(LocalizedStringDTO description) {
		this.description = description;
	}

	public LocalizedStringDTO getBenefitPeriod() {
		return benefitPeriod;
	}

	public void setBenefitPeriod(LocalizedStringDTO benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}

}
