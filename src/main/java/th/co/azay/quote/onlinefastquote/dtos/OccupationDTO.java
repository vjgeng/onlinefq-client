package th.co.azay.quote.onlinefastquote.dtos;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OccupationDTO {

	private String code;
	private LocalizedStringDTO name = new LocalizedStringDTO();
	private String riskLevel;
	private List<OccupationDTO> occupations = new ArrayList<>();

	@JsonInclude(Include.ALWAYS)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonInclude(Include.ALWAYS)
	public String getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}

	@JsonInclude(Include.ALWAYS)
	public LocalizedStringDTO getName() {
		return name;
	}

	public void setName(LocalizedStringDTO name) {
		this.name = name;
	}

	@JsonInclude(Include.NON_EMPTY)
	public List<OccupationDTO> getOccupations() {
		return occupations;
	}

	public void setOccupations(List<OccupationDTO> occupations) {
		this.occupations = occupations;
	}

	public void setName(String languageCode, String translation) {
		name.setTranslation(languageCode, translation);
	}

	@Override
	public String toString() {
		return "OccupationDTO [code=" + code + ", name=" + name + ", riskLevel=" + riskLevel + ", occupations=" + occupations + "]";
	}

}
