
package th.co.azay.quote.onlinefastquote.dtos.findproduct;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "enabled",
    "display",
    "minAge",
    "maxAge",
    "paymentFrequency",
    "blacklists",
    "rules"
})
public class ProductDetail {

    @JsonProperty("code")
    private String code;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("display")
    private Boolean display;
    @JsonProperty("minAge")
    private Integer minAge;
    @JsonProperty("maxAge")
    private Integer maxAge;
    @JsonProperty("paymentFrequency")
    private String paymentFrequency;
    @JsonProperty("blacklists")
    private Blacklists blacklists;
    @JsonProperty("rules")
    private List<Rule> rules = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("display")
    public Boolean getDisplay() {
        return display;
    }

    @JsonProperty("display")
    public void setDisplay(Boolean display) {
        this.display = display;
    }

    @JsonProperty("minAge")
    public Integer getMinAge() {
        return minAge;
    }

    @JsonProperty("minAge")
    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    @JsonProperty("maxAge")
    public Integer getMaxAge() {
        return maxAge;
    }

    @JsonProperty("maxAge")
    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    @JsonProperty("paymentFrequency")
    public String getPaymentFrequency() {
        return paymentFrequency;
    }

    @JsonProperty("paymentFrequency")
    public void setPaymentFrequency(String paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    @JsonProperty("blacklists")
    public Blacklists getBlacklists() {
        return blacklists;
    }

    @JsonProperty("blacklists")
    public void setBlacklists(Blacklists blacklists) {
        this.blacklists = blacklists;
    }

    @JsonProperty("rules")
    public List<Rule> getRules() {
        return rules;
    }

    @JsonProperty("rules")
    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
