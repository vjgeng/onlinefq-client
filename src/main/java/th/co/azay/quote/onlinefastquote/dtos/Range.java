package th.co.azay.quote.onlinefastquote.dtos;

import java.math.BigDecimal;

public class Range {

	private BigDecimal min;
	private BigDecimal max;

	public BigDecimal getMin() {
		return min;
	}

	public void setMin(BigDecimal min) {
		this.min = min;
	}

	public BigDecimal getMax() {
		return max;
	}

	public void setMax(BigDecimal max) {
		this.max = max;
	}

}
