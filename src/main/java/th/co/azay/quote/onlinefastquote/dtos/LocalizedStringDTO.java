package th.co.azay.quote.onlinefastquote.dtos;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

public class LocalizedStringDTO {

	private Map<String, String> translations = new HashMap<>();

	@JsonAnyGetter
	public Map<String, String> getTranslations() {
		return translations;
	}

	@JsonAnySetter
	public void setTranslation(String languageCode, String text) {
		translations.put(languageCode, text);
	}

	@Override
	public String toString() {
		return "LocalizedStringDTO [translations=" + translations + "]";
	}

}
