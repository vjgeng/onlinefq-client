package th.co.azay.quote.onlinefastquote.dtos;

import java.time.ZonedDateTime;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolutionDTO {

	private String code;
	private String name;
	private String agentId;

	private SolutionStatus status;
	private double totalPremium;
	private PaymentFrequency paymentFrequency;
	private String currency;

	private ZonedDateTime createdDate;
	private ZonedDateTime lastModifiedDate;

	private Boolean favorite;
	private NoteDTO note;
	private String coverImage;
	@JsonProperty("package")
	private String packagesName;
	private ReminderDTO reminder;
	private Map<String, PersonDTO> persons;
	private Map<String, PlanDTO> plans;
	private String policyHolder;
	private Map<String, String> extendedProperties;
	private Map<String, SolutionModuleDTO> modules;
	private Map<String, SolutionNeedDTO> needs;
	private SalesIllustrationDTO salesIllustration;

	private String selectedPackage;

	private Map<String, Object> availabilities;

	private SolutionCache cache;
	private boolean toBeSynced;

	@JsonInclude(Include.ALWAYS)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonInclude(Include.ALWAYS)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonInclude(Include.ALWAYS)
	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	@JsonInclude(Include.ALWAYS)
	public SolutionStatus getStatus() {
		return status;
	}

	public void setStatus(SolutionStatus status) {
		this.status = status;
	}

	@JsonInclude(Include.ALWAYS)
	public double getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(double totalPremium) {
		this.totalPremium = totalPremium;
	}

	@JsonInclude(Include.ALWAYS)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@JsonInclude(Include.ALWAYS)
	public PaymentFrequency getPaymentFrequency() {
		return paymentFrequency;
	}

	public void setPaymentFrequency(PaymentFrequency paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}

	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}

	@JsonInclude(Include.ALWAYS)
	@JsonSerialize(using = CustomZonedDateTimeSerializer.class)
	public ZonedDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	@JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
	public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@JsonInclude(Include.ALWAYS)
	public Boolean getFavorite() {
		return favorite;
	}

	public void setFavorite(Boolean favorite) {
		this.favorite = favorite;
	}

	@JsonInclude(Include.ALWAYS)
	public NoteDTO getNote() {
		return note;
	}

	public void setNote(NoteDTO note) {
		this.note = note;
	}

	@JsonInclude(Include.ALWAYS)
	public String getCoverImage() {
		return coverImage;
	}

	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}

	@JsonInclude(Include.ALWAYS)
	public ReminderDTO getReminder() {
		return reminder;
	}

	public void setReminder(ReminderDTO reminder) {
		this.reminder = reminder;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, PersonDTO> getPersons() {
		return persons;
	}

	public void setPersons(Map<String, PersonDTO> persons) {
		this.persons = persons;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, PlanDTO> getPlans() {
		return plans;
	}

	public void setPlans(Map<String, PlanDTO> plans) {
		this.plans = plans;
	}

	@JsonInclude(Include.ALWAYS)
	public String getPolicyHolder() {
		return policyHolder;
	}

	public void setPolicyHolder(String policyHolder) {
		this.policyHolder = policyHolder;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, String> getExtendedProperties() {
		return extendedProperties;
	}

	public void setExtendedProperties(Map<String, String> extendedProperties) {
		this.extendedProperties = extendedProperties;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, SolutionModuleDTO> getModules() {
		return modules;
	}

	public void setModules(Map<String, SolutionModuleDTO> modules) {
		this.modules = modules;
	}

	@JsonInclude(Include.ALWAYS)
	public Map<String, SolutionNeedDTO> getNeeds() {
		return needs;
	}

	public void setNeeds(Map<String, SolutionNeedDTO> needs) {
		this.needs = needs;
	}

	@JsonInclude(Include.ALWAYS)
	public SalesIllustrationDTO getSalesIllustration() {
		return salesIllustration;
	}

	public void setSalesIllustration(SalesIllustrationDTO salesIllustration) {
		this.salesIllustration = salesIllustration;
	}

	@JsonInclude(Include.ALWAYS)
	public String getSelectedPackage() {
		return selectedPackage;
	}

	public void setSelectedPackage(String selectedPackage) {
		this.selectedPackage = selectedPackage;
	}

	public Map<String, Object> getAvailabilities() {
		return availabilities;
	}

	public void setAvailabilities(Map<String, Object> availabilities) {
		this.availabilities = availabilities;
	}

	public SolutionCache getCache() {
		return cache;
	}

	public void setCache(SolutionCache cache) {
		this.cache = cache;
	}

	public boolean isToBeSynced() {
		return toBeSynced;
	}

	public void setToBeSynced(boolean toBeSynced) {
		this.toBeSynced = toBeSynced;
	}

	public String getPackagesName() {
		return packagesName;
	}

	public void setPackagesName(String packagesName) {
		this.packagesName = packagesName;
	}

	@Override
	public String toString() {
		return "SolutionDTO [code=" + code + ", name=" + name + ", agentId=" + agentId + ", status=" + status + ", totalPremium=" + totalPremium
				+ ", paymentFrequency=" + paymentFrequency + ", currency=" + currency + ", createdDate=" + createdDate + ", lastModifiedDate="
				+ lastModifiedDate + ", favorite=" + favorite + ", note=" + note + ", coverImage=" + coverImage + ", reminder=" + reminder
				+ ", persons=" + persons + ", plans=" + plans + ", policyHolder=" + policyHolder + ", extendedProperties=" + extendedProperties
				+ ", modules=" + modules + ", needs=" + needs + ", salesIllustration=" + salesIllustration + ", selectedPackage=" + selectedPackage
				+ ", availabilities=" + availabilities + ", cache=" + cache + ", toBeSynced=" + toBeSynced + ", package=" + packagesName+ "]";
	}

}
