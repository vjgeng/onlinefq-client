package th.co.azay.quote.onlinefastquote.dtos;

import java.util.Map;

public class SolutionCache extends ModuleCache {

	private Map<String, TableCache> tables;

	private Map<String, ModuleCache> modules;

	public Map<String, TableCache> getTables() {
		return tables;
	}

	public void setTables(Map<String, TableCache> tables) {
		this.tables = tables;
	}

	public Map<String, ModuleCache> getModules() {
		return modules;
	}

	public void setModules(Map<String, ModuleCache> cache) {
		this.modules = cache;
	}

}
