package th.co.azay.quote.onlinefastquote.dtos;

import java.math.BigDecimal;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = AgeDetailDTO.Builder.class)
public class AgeDetailDTO {

	private final BigDecimal min;
	private final BigDecimal max;

	public BigDecimal getMin() {
		return min;
	}

	public BigDecimal getMax() {
		return max;
	}

	public AgeDetailDTO(Builder builder) {
		this.min = builder.min;
		this.max = builder.max;
	}

	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "with")
	public static class Builder {

		private BigDecimal min;
		private BigDecimal max;

		public Builder withMin(BigDecimal min) {
			this.min = min;
			return this;
		}

		public Builder withMax(BigDecimal max) {
			this.max = max;
			return this;
		}

		public AgeDetailDTO build() {
			return new AgeDetailDTO(this);
		}
	}

	@Override
	public String toString() {
		return "AgeDetailDTO [min=" + min + ", max=" + max + "]";
	}

}
