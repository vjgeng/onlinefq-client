package th.co.azay.quote.onlinefastquote.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RangeDTO {

	private long min;
	private long max;

	@JsonInclude(Include.ALWAYS)
	public long getMin() {
		return min;
	}

	public void setMin(long min) {
		this.min = min;
	}

	@JsonInclude(Include.ALWAYS)
	public long getMax() {
		return max;
	}

	public void setMax(long max) {
		this.max = max;
	}

}
