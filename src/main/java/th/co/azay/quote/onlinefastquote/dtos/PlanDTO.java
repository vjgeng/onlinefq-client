package th.co.azay.quote.onlinefastquote.dtos;

import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanDTO {

	private String code;
	private Set<FundSelectionDTO> fundSelections;
	private Set<KYCAnswerDTO> kycAnswers;

	@JsonInclude(Include.ALWAYS)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonInclude(Include.ALWAYS)
	public Set<FundSelectionDTO> getFundSelections() {
		return fundSelections;
	}

	public void setFundSelections(Set<FundSelectionDTO> fundSelections) {
		this.fundSelections = fundSelections;
	}

	@JsonInclude(Include.ALWAYS)
	public Set<KYCAnswerDTO> getKycAnswers() {
		return kycAnswers;
	}

	public void setKycAnswers(Set<KYCAnswerDTO> kycAnswers) {
		this.kycAnswers = kycAnswers;
	}

}
