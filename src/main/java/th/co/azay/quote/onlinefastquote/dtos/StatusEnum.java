package th.co.azay.quote.onlinefastquote.dtos;

public enum StatusEnum {

	ACTIVE("A"), INACTIVE("I");

	private final String value;

	StatusEnum(final String v) {
		this.value = v;
	}

	public String value() {
		return this.value;
	}

	public static StatusEnum fromValue(final String v) {

		for (final StatusEnum c : StatusEnum.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(String.valueOf(v));
	}

}
