package th.co.azay.quote.onlinefastquote.dtos;

public class DRTVSolutionWrapperDTO {
    private final DRTVPackageJsonDTO packageData;

    public DRTVSolutionWrapperDTO(Builder builder) {
        this.packageData = builder.packageData;
    }

    public DRTVPackageJsonDTO getPackageData() {
        return packageData;
    }

    public static final class Builder {
        private DRTVPackageJsonDTO packageData;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder withPackageData(DRTVPackageJsonDTO packageData) {
            this.packageData = packageData;
            return this;
        }

        public DRTVSolutionWrapperDTO build() {
            return new DRTVSolutionWrapperDTO(this);
        }
    }
}
