package th.co.azay.quote.onlinefastquote.dtos;

public class DRTVPackageKey {
    private String packageCode;
    private int age;
    private Gender gender;

    public DRTVPackageKey(String packageCode, int age, Gender gender) {
        this.packageCode = packageCode;
        this.age = age;
        this.gender = gender;
    }

    public DRTVPackageKey() {
        // blank constructor required to initialize the class by jackson.
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DRTVPackageKey key = (DRTVPackageKey) o;

        if (age != key.age) {
            return false;
        }
        if (!packageCode.equals(key.packageCode)) {
            return false;
        }
        return gender == key.gender;
    }

    @Override
    public int hashCode() {
        int result = packageCode.hashCode();
        result = 31 * result + age;
        return 31 * result + gender.hashCode();
    }

    @Override
    public String toString() {
        return "DRTVPackageKey{" +
                "packageCode='" + packageCode + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }
}
