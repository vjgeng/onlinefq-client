package th.co.azay.quote.onlinefastquote;

import java.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import th.co.azay.quote.onlinefastquote.dtos.Gender;
import th.co.azay.quote.onlinefastquote.dtos.SolutionDTO;
import th.co.azay.quote.onlinefastquote.dtos.SolutionWrapperDTO;
import th.co.azay.quote.onlinefastquote.dtos.findproduct.ProductDetail;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {OnlineFastQuoteServiceImpl.class, OnlineFastQuoteHandler.class})
public class CreateQuoteTest {
    @Autowired
    OnlineFastQuoteServiceImpl onlineFastQuoteService;

    private static final String FIND_PRODUCT_URL = "https://api-test.allianz.com/public-module-qa/services/rest/findProduct"; //productCode=HEALTH_KIDS
    private static final String CREATE_SOLUTION_URL = "https://api-test.allianz.com/public-module-dev/services/rest/solutions/riders";
    private static final String CALCULATE_PREMIUM_URL = "https://api-test.allianz.com/public-module-dev/services/rest/solutions/calculateWithRiderAndAdditionalRider";


    @Test
    public void findProductSuccess() throws Exception{
        ProductDetail product = onlineFastQuoteService.findProduct(FIND_PRODUCT_URL, "HEALTH_KIDS");
        Assert.assertNotNull(product);
        Assert.assertEquals(product.getCode(), "HEALTH_KIDS");
        Assert.assertNotEquals(product.getCode(), "WHATEVER");
    }


    @Test
    public void createSolutionSuccess() throws Exception{
        SolutionDTO solution = onlineFastQuoteService.createSolution(CREATE_SOLUTION_URL, "online", "TH", "685", "HEALTH_KIDS");
        Assert.assertNotNull(solution);
        Assert.assertEquals(solution.getAgentId(), "public-module");
    }

    @Test
    public void calculatePremiumSuccess() throws Exception{
        SolutionDTO solution = onlineFastQuoteService.createSolution(CREATE_SOLUTION_URL, "online", "TH", "685", "HEALTH_KIDS");
        solution.setPackagesName("HEALTH_RETIRE");
        onlineFastQuoteService.calculatePremium(CALCULATE_PREMIUM_URL, "online", "TH", "685", solution);

    }

    @Test
    public void calculatePremiumHEALTH_RETIREMale40Success() throws Exception{
        ProductDetail product = onlineFastQuoteService.findProduct(FIND_PRODUCT_URL, "HEALTH_KIDS");
        SolutionDTO solution = onlineFastQuoteService.createSolution(CREATE_SOLUTION_URL, "online", "TH", "685", "HEALTH_KIDS");
        solution.setPackagesName("HEALTH_RETIRE");
        solution.getPersons().get("INSURED").setGender(Gender.M);
        solution.getPersons().get("INSURED").setDateOfBirth(LocalDate.now().plusYears(-40));

        String occupationCode = ("n/a".equalsIgnoreCase(product.getRules().get(0).getOccupation()))?"A10101":product.getRules().get(0).getOccupation();
        solution.getPersons().get("INSURED").getOccupation().setCode(occupationCode);
        //solution.getModules().get("MWLA9021").setSelected(true);
        //solution.getModules().get("CI48").setSelected(true);
        //solution.getModules().get("CBN").setSelected(true);
        //solution.getModules().get("HSS").setSelected(true);
        SolutionWrapperDTO baseSolution = onlineFastQuoteService.calculatePremium(CALCULATE_PREMIUM_URL, "online", "TH", "685", solution);

        Assert.assertNotNull(baseSolution);
        Assert.assertEquals(baseSolution.getTotalPremiumAnnual(), new BigDecimal("27548"));

    }

    @Test
    public void calculatePremiumHEALTH_KIDSMale2Success() throws Exception{
        ProductDetail product = onlineFastQuoteService.findProduct(FIND_PRODUCT_URL, "HEALTH_KIDS");
        SolutionDTO solution = onlineFastQuoteService.createSolution(CREATE_SOLUTION_URL, "online", "TH", "685", "HEALTH_KIDS");
        solution.setPackagesName("HEALTH_KIDS");
        solution.getPersons().get("INSURED").setGender(Gender.M);
        solution.getPersons().get("INSURED").setDateOfBirth(LocalDate.now().plusYears(-2));

        String occupationCode = ("n/a".equalsIgnoreCase(product.getRules().get(0).getOccupation()))?"A10101":product.getRules().get(0).getOccupation();
        solution.getPersons().get("INSURED").getOccupation().setCode(occupationCode);
        //solution.getModules().get("MWLA9021").setSelected(true);
        //solution.getModules().get("CI48").setSelected(true);
        //solution.getModules().get("CBN").setSelected(true);
        //solution.getModules().get("HSS").setSelected(true);
        SolutionWrapperDTO baseSolution = onlineFastQuoteService.calculatePremium(CALCULATE_PREMIUM_URL, "online", "TH", "685", solution);

        Assert.assertNotNull(baseSolution);
        Assert.assertEquals(baseSolution.getTotalPremiumAnnual(), new BigDecimal("40576"));

    }

    @Test
    public void calculatePremiumHEALTH_OPDMale40Success() throws Exception{
        ProductDetail product = onlineFastQuoteService.findProduct(FIND_PRODUCT_URL, "HEALTH_OPD");
        SolutionDTO solution = onlineFastQuoteService.createSolution(CREATE_SOLUTION_URL, "online", "TH", "685", "HEALTH_KIDS");
        solution.setPackagesName("HEALTH_OPD");
        solution.getPersons().get("INSURED").setGender(Gender.M);
        solution.getPersons().get("INSURED").setDateOfBirth(LocalDate.now().plusYears(-42));

        String occupationCode = ("n/a".equalsIgnoreCase(product.getRules().get(0).getOccupation()))?"A10101":product.getRules().get(0).getOccupation();
        solution.getPersons().get("INSURED").getOccupation().setCode(occupationCode);
        //solution.getModules().get("MWLA9021").setSelected(true);
        //solution.getModules().get("CI48").setSelected(true);
        //solution.getModules().get("CBN").setSelected(true);
        //solution.getModules().get("HSS").setSelected(true);
        SolutionWrapperDTO baseSolution = onlineFastQuoteService.calculatePremium(CALCULATE_PREMIUM_URL, "online", "TH", "685", solution);

        Assert.assertNotNull(baseSolution);
        Assert.assertEquals(baseSolution.getTotalPremiumAnnual(), new BigDecimal("21811"));

    }
}
